# Using GitLab CI with pandoc/latex

The [pandoc/latex Docker Image](https://hub.docker.com/r/pandoc/latex) changed its entrypoint in the last version (2.10), so to be able to use this image in your GitLab CI you have to specify the entrypoint like this:

```yaml
image:
  name: pandoc/latex
  entrypoint: ["/bin/sh", "-c"]
```

For a full example, take a look at the [.gitlab-ci.yml](.gitlab-ci.yml).